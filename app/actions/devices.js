// @flow

import * as types from '../constants/action-types';

export const addDevice = device => ({ type: types.ADD_DEVICE, payload: device });
export const toggleDevice = device => ({ type: types.TOGGLE_DEVICE, payload: device });
