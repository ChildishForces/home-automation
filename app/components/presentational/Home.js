// @flow
import React, { Component } from 'react';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
// import store from '../../store';
// import { Link } from 'react-router-dom';
// import styles from './Home.css';

type Props = {};


export default class Home extends Component<Props> {
  props: Props;

  render() {
    return (
      <div style={{ padding: 20 }}>
        <div data-tid="container">
          <Typography variant="title">
            Available Devices
          </Typography>
          <Button onClick={() => { console.log('onClick'); }}>Click Me!</Button>
        </div>
      </div>
    );
  }
}
