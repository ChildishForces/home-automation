// @flow
import * as React from 'react';
import MainNav from './MainNav';

type Props = {
  children: React.Node
};

export default class App extends React.Component<Props> {
  props: Props;

  render() {
    return (
      <div>
        <MainNav />
        {this.props.children}
      </div>
    );
  }
}
