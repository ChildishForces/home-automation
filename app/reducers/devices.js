import * as types from '../constants/action-types';

type actionType = {
  +type: string
};

function devices(state: Array = [], action: actionType) {
  switch (action.type) {
    case types.ADD_DEVICE:
      return [
        ...state,
        action.payload,
      ];
    default:
      return state;
  }
}

export default devices;
