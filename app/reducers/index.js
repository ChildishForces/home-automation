// @flow
import { combineReducers } from 'redux';
import { routerReducer as router } from 'react-router-redux';

import devices from '../reducers/devices';

const rootReducer = combineReducers({
  devices,
  router,
});

export default rootReducer;
